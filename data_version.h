#ifndef DATA_VERSION_H
#define DATA_VERSION_H

enum class DataSize : unsigned char
{
    vNull = 0x00,
    vByte = 0x01
};

enum class DataCode : unsigned char
{
    vNull = 0x00,
    vController = 0xC1
};

enum class DataVersion : unsigned char
{
    vNull = 0x00,
    v1 = 0x01
};

enum class DataComment : unsigned char
{
    vNull = 0x00,
    vRelease = 0x01,
    vDevelopment = 0xFF
};

#endif // DATA_VERSION_H

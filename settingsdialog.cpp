#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include <QSettings>

SettingsDialog::SettingsDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);
    this->loadSettings();
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

double SettingsDialog::maxPower() const
{
    return ui->in_MaxPower->value();
}

void SettingsDialog::setMaxPower(double power)
{
    ui->in_MaxPower->setValue(power);
}

double SettingsDialog::initialPower() const
{
    return ui->in_InitialPower->value();
}

void SettingsDialog::setInitialPower(double power)
{
    ui->in_InitialPower->setValue(power);
}

double SettingsDialog::warningRange() const
{
    return ui->in_WarningRange->value();
}

void SettingsDialog::setWarningRange(double range)
{
    ui->in_WarningRange->setValue(range);
}

double SettingsDialog::criticalRange() const
{
    return ui->in_CriticalRange->value();
}

void SettingsDialog::setCriticalRange(double range)
{
    ui->in_CriticalRange->setValue(range);
}

void SettingsDialog::loadSettings()
{
    QSettings settings;

    this->setMaxPower(settings.value("max-power", 0.0).toDouble());
    this->setInitialPower(settings.value("initial-power", 0.0).toDouble());
    this->setWarningRange(settings.value("warning-range", 60.0).toDouble());
    this->setCriticalRange(settings.value("critical-range", 80.0).toDouble());
}

void SettingsDialog::accept()
{
    QSettings settings;

    settings.setValue("max-power", this->maxPower());
    settings.setValue("initial-power", this->initialPower());
    settings.setValue("warning-range", this->warningRange());
    settings.setValue("critical-range", this->criticalRange());

    QDialog::accept();
}

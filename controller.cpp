#include "controller.h"
#include "controllereditor.h"
#include "data_version.h"

Controller::Controller(QObject* parent) :
    QObject(parent),
    m_power(0.0),
    m_editor(0),
    m_deleteEditor(false),
    m_state(ControllerState::Invalid),
    m_powered(false),
    m_ip(0),
    m_port(0),
    m_pin(0),
    m_duplicate(false),
    m_errorCount(0),
    m_errorThreshold(2)
{
    this->setupSignals();
    m_sync.start(5000);
}

Controller::~Controller()
{
    if (m_deleteEditor)
    {
        this->freeEditor();
    }
}

QString Controller::id() const
{
    return m_id;
}

void Controller::setId(const QString& id)
{
    m_id = id;
}

QString Controller::image() const
{
    return m_image;
}

void Controller::setImage(const QString& image)
{
    m_image = image;
}

QString Controller::description() const
{
    return m_description;
}

void Controller::setDescription(const QString& description)
{
    m_description = description;
}

double Controller::power() const
{
    return m_power;
}

void Controller::setPower(double power)
{
    m_power = power;
}

quint32 Controller::ip() const
{
    return m_ip;
}

void Controller::setIp(quint32 ip)
{
    m_ip = ip;
}

quint16 Controller::port() const
{
    return m_port;
}

void Controller::setPort(quint16 port)
{
    m_port = port;
}

quint8 Controller::pin() const
{
    return m_pin;
}

void Controller::setPin(quint8 target)
{
    m_pin = target;
}

bool Controller::duplicate() const
{
    return m_duplicate;
}

void Controller::setDuplicate(bool duplicate)
{
    m_duplicate = duplicate;
}

ControllerEditor* Controller::editor()
{
    if (!m_editor)
    {
        m_editor = new ControllerEditor(0);
    }

    if (m_editor)
    {
        m_editor->disableDeletion();
        m_editor->setController(this);
        m_editor->reloadController();
    }

    return m_editor;
}

void Controller::setEditor(ControllerEditor* editor, bool deleteOld)
{
    if (deleteOld)
    {
        this->freeEditor();
    }

    m_editor = editor;
    m_deleteEditor = deleteOld;
}

void Controller::freeEditor()
{
    if (m_editor)
    {
        m_editor->deleteLater();
        m_editor = 0;
    }

    m_deleteEditor = false;
}

QByteArray Controller::exportData() const
{
    QByteArray data;
    QDataStream stream(&data, QIODevice::WriteOnly);

    this->exportDataToStream(stream);

    return data;
}

bool Controller::importData(const QByteArray& data)
{
    QDataStream stream(data);
    return this->importDataFromStream(stream);
}

void Controller::exportDataToStream(QDataStream& stream) const
{
    uchar size = static_cast<uchar>(DataSize::vByte);
    stream << size;

    uchar classCode = static_cast<uchar>(DataCode::vController);
    uchar classVersion = static_cast<uchar>(DataVersion::v1);
    uchar classComment = static_cast<uchar>(DataComment::vRelease);
    stream << classCode << classVersion << classComment;

    stream.setVersion(QDataStream::Qt_5_2);

    stream << m_id;
    stream << m_image;
    stream << m_description;
    stream << m_power;
    stream << m_ip;
    stream << m_port;
    stream << m_pin;
}

bool Controller::importDataFromStream(QDataStream& stream)
{
    uchar size = static_cast<uchar>(DataSize::vNull);
    stream >> size;

    int dataVersion = 0;

    if (size == static_cast<uchar>(DataSize::vByte))
    {
        uchar classCode = static_cast<uchar>(DataCode::vNull);
        uchar classVersion = static_cast<uchar>(DataVersion::vNull);
        uchar classComment = static_cast<uchar>(DataComment::vNull);

        stream >> classCode;

        if (classCode == static_cast<uchar>(DataCode::vController))
        {
            stream >> classVersion;

            if (classVersion == static_cast<uchar>(DataVersion::v1))
            {
                stream >> classComment;
                stream.setVersion(QDataStream::Qt_5_2);

                dataVersion = 1;
            }
        }
    }

    if (dataVersion == 0)
    {
        return false;
    }

    if (dataVersion >= 1)
    {
        stream >> m_id;
        stream >> m_image;
        stream >> m_description;
        stream >> m_power;
        stream >> m_ip;
        stream >> m_port;
        stream >> m_pin;
    }

    return true;
}

void Controller::syncPinState()
{
    if (m_duplicate)
    {
        m_state = ControllerState::Invalid;
        emit stateUpdated(m_state);
        emit messageUpdated(tr("Configuração de rede inválida."));

        if (m_powered)
        {
            m_powered = false;
        }

        return;
    }

    if (m_syncTimeout.isValid() && m_syncTimeout.hasExpired(5000))
    {
        this->syncError(m_pin);
    }
    else
    {
        m_state = ControllerState::Updating;
        emit stateUpdated(m_state);
        emit messageUpdated(tr("Sincronizando..."));
    }

    bool success = m_arduino.requestPinState(QHostAddress(m_ip), m_port, m_pin);

    if (!success)
    {
        m_state = ControllerState::Invalid;
        emit stateUpdated(m_state);
        emit messageUpdated(tr("Erro ao sincronizar."));

        m_syncTimeout.start();
    }
}

bool Controller::turnOn()
{    
    if (m_duplicate)
    {
        m_state = ControllerState::Invalid;
        emit stateUpdated(m_state);
        emit messageUpdated(tr("Configuração de rede inválida."));

        if (m_powered)
        {
            m_powered = false;
        }

        return false;
    }

    bool success = false;

    if (m_state == ControllerState::Unpowered)
    {
        success = m_arduino.updatePinPower(QHostAddress(m_ip), m_port, m_pin, true);

        if (success)
        {
            m_state = ControllerState::Updating;
            emit stateUpdated(m_state);
            emit messageUpdated(tr("Energizando circuito..."));
        }
    }

    return success;
}

bool Controller::turnOff()
{
    if (m_duplicate)
    {
        m_state = ControllerState::Invalid;
        emit stateUpdated(m_state);
        emit messageUpdated(tr("Configuração de rede inválida."));

        if (m_powered)
        {
            m_powered = false;
        }

        return false;
    }

    bool success = false;

    if (m_state == ControllerState::Powered)
    {
        success = m_arduino.updatePinPower(QHostAddress(m_ip), m_port, m_pin, false);

        if (success)
        {
            m_state = ControllerState::Updating;
            emit stateUpdated(m_state);
            emit messageUpdated(tr("Desligando circuito..."));
        }
    }

    return success;
}

bool Controller::isPowered() const
{
    bool powered = (this->isValid() && (m_state == ControllerState::Powered));

    if (!powered)
    {
        powered = (!m_duplicate && m_state == ControllerState::Updating && m_powered);
    }

    return powered;
}

bool Controller::isValid() const
{
    return (m_state == ControllerState::Powered || m_state == ControllerState::Unpowered) && !m_duplicate;
}

ControllerState Controller::state() const
{
    return m_state;
}

void Controller::pinState(quint8 pin, bool state)
{
    if (pin == m_pin)
    {
        m_state = (state ? ControllerState::Powered : ControllerState::Unpowered);
        emit stateUpdated(m_state);
        emit messageUpdated(state ? tr("Circuito energizado.") : tr("Circuito desligado."));

        m_powered = state;

        m_syncTimeout.start();

        if (m_errorCount > 0)
        {
            m_errorCount = 0;
        }
    }
    else
    {
        this->syncError(pin);
    }
}

void Controller::syncError(quint8 target)
{
    Q_UNUSED(target);

    bool reportError = false;

    if (m_powered)
    {
        m_errorCount++;

        if (m_errorCount >= m_errorThreshold)
        {
            reportError = true;

            if (m_errorCount > 0)
            {
                m_errorCount = 0;
            }
        }
    }

    if (reportError)
    {
        m_state = ControllerState::Invalid;
        emit stateUpdated(m_state);
        emit messageUpdated(tr("Erro ao sincronizar."));

        if (m_powered)
        {
            m_powered = false;
        }
    }

    m_syncTimeout.start();
}

void Controller::setupSignals()
{
    connect(&m_arduino, &ArduinoNetwork::pinStateResponse, this, &Controller::pinState);
    connect(&m_arduino, &ArduinoNetwork::socketFailed, this, &Controller::syncError);

    connect(&m_sync, &QTimer::timeout, this, &Controller::syncPinState);
}

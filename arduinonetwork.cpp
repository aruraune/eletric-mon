#include "arduinonetwork.h"
#include <QTcpSocket>
#include "arduino_protocol.h"

ArduinoNetwork::ArduinoNetwork(QObject* parent) :
    QObject(parent)
{
}

ArduinoNetwork::~ArduinoNetwork()
{
}

bool ArduinoNetwork::requestPinState(const QHostAddress& address, quint16 port, quint8 pin)
{
    bool success = false;

    QTcpSocket* socket = new QTcpSocket(this);

    if (socket)
    {
        socket->setProperty("arduino-pin", pin);
        socket->setProperty("arduino-reply", 0);

        connect(socket, &QTcpSocket::connected, this, &ArduinoNetwork::pinStateConnected);
        connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(socketError(QAbstractSocket::SocketError)));
        connect(socket, &QTcpSocket::readyRead, this, &ArduinoNetwork::socketDataReceived);
        connect(socket, &QTcpSocket::disconnected, this, &ArduinoNetwork::socketDisconnected);

        socket->connectToHost(address, port);

        success = true;
    }

    return success;
}

bool ArduinoNetwork::updatePinPower(const QHostAddress& address, quint16 port, quint8 pin, bool power)
{
    bool success = false;

    QTcpSocket* socket = new QTcpSocket(this);

    if (socket)
    {
        socket->setProperty("arduino-pin", pin);
        socket->setProperty("arduino-pin-power", power);
        socket->setProperty("arduino-reply", 0);

        connect(socket, &QTcpSocket::connected, this, &ArduinoNetwork::updatePowerConnected);
        connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(socketError(QAbstractSocket::SocketError)));
        connect(socket, &QTcpSocket::readyRead, this, &ArduinoNetwork::socketDataReceived);
        connect(socket, &QTcpSocket::disconnected, this, &ArduinoNetwork::socketDisconnected);

        socket->connectToHost(address, port);

        success = true;
    }

    return success;
}


void ArduinoNetwork::pinStateConnected()
{
    QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());

    if (socket)
    {
        bool success = false;

        quint8 pin = socket->property("arduino-pin").toUInt(&success);

        if (!success)
        {
            socket->disconnectFromHost();
            return;
        }

        QString data("[%1-%2-%3]");

        data = data.arg(static_cast<quint16>(Protocol::PortStatus), 4, 10, QChar('0'));
        data = data.arg(pin, 4, 10, QChar('0'));
        data = data.arg(0, 4, 10, QChar('0'));

        socket->write(data.toLatin1());
    }
}

void ArduinoNetwork::updatePowerConnected()
{
    QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());

    if (socket)
    {
        bool success = false;

        quint8 pin = socket->property("arduino-pin").toUInt(&success);

        if (!success)
        {
            socket->disconnectFromHost();
            return;
        }

        bool power = socket->property("arduino-pin-power").toBool();

        QString data("[%1-%2-%3]");

        data = data.arg(static_cast<quint16>(Protocol::PortPower), 4, 10, QChar('0'));
        data = data.arg(pin, 4, 10, QChar('0'));
        data = data.arg((power ? 1 : 0), 4, 10, QChar('0'));

        socket->write(data.toLatin1());
    }
}

void ArduinoNetwork::socketError(QAbstractSocket::SocketError error)
{
    Q_UNUSED(error);

    QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());

    if (socket)
    {
        this->socketDisconnected();
    }
}

void ArduinoNetwork::socketDataReceived()
{
    QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());

    if (socket)
    {
        while (socket->bytesAvailable() >= 16)
        {
            QByteArray rawState = socket->read(16);

            if (rawState.isEmpty())
            {
                continue;
            }

            QString state = QString::fromLatin1(rawState);
            QRegExp data("^\\[([0-9]{4})-([0-9]{4})-([0-9]{4})\\]$");

            if (!data.exactMatch(state))
            {
                continue;
            }

            QStringList fields = data.capturedTexts();

            if (fields.isEmpty())
            {
                continue;
            }

            fields.removeFirst();

            if (fields.count() < 3)
            {
                continue;
            }

            quint16 packetType = fields.value(0, "0").toUShort();
            quint16 pinId = fields.value(1, "0").toUShort();
            quint16 packetData = fields.value(2, "0").toUShort();

            if (packetType == static_cast<quint16>(Protocol::PortStatus))
            {
                if (pinId != socket->property("arduino-pin").toUInt())
                {
                    continue;
                }

                socket->setProperty("arduino-reply", 1);

                emit pinStateResponse(pinId, (packetData == 1));

                break;
            }
        }
    }
}

void ArduinoNetwork::socketDisconnected()
{
    QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());

    if (socket)
    {
        if (socket->property("arduino-reply").toUInt() == 0)
        {
            emit socketFailed(socket->property("arduino-pin").toUInt());
        }

        socket->deleteLater();
        socket = 0;
    }
}

#include "ac_fields.h"
#include <QObject>
#include <QSettings>

QMap<AC, bool> AC_load(const QString& user)
{
    QMap<AC, bool> ac;

    ac[AC::LOGIN] = AC_loadValue(user, AC::LOGIN);
    ac[AC::MANAGE] = AC_loadValue(user, AC::MANAGE);
    ac[AC::SETTINGS] = AC_loadValue(user, AC::SETTINGS);
    ac[AC::USERS] = AC_loadValue(user, AC::USERS);
    ac[AC::POWER] = AC_loadValue(user, AC::POWER);
    ac[AC::ADMIN] = AC_loadValue(user, AC::ADMIN);

    return ac;
}

void AC_save(const QMap<AC, bool>& ac, const QString& user)
{
    QMap<AC, bool>::ConstIterator it = ac.begin();

    for (; it != ac.end(); ++it)
    {
        AC_saveValue(user, it.key(), it.value());
    }
}

QString AC_name(AC ac)
{
    switch (ac)
    {
        case AC::LOGIN:
            return QObject::tr("login");

        case AC::MANAGE:
            return QObject::tr("manage");

        case AC::SETTINGS:
            return QObject::tr("settings");

        case AC::USERS:
            return QObject::tr("users");

        case AC::POWER:
            return QObject::tr("power");

        case AC::ADMIN:
            return QObject::tr("admin");
    }

    return QObject::tr("Permissão não reconhecida.");
}

QString AC_description(AC ac)
{
    switch (ac)
    {
        case AC::LOGIN:
            return QObject::tr("Acessar o sistema.");

        case AC::MANAGE:
            return QObject::tr("Gerenciar os circuitos.");

        case AC::SETTINGS:
            return QObject::tr("Editar as configurações.");

        case AC::USERS:
            return QObject::tr("Editar os usuários.");

        case AC::POWER:
            return QObject::tr("Energizar ou desligar os circuitos.");

        case AC::ADMIN:
            return QObject::tr("É administrador.");
    }

    return QObject::tr("Permissão não reconhecida.");
}

bool AC_loadValue(const QString& user, AC ac)
{
    QSettings settings;
    return settings.value(QString("u_%1/ac_%2").arg(user, AC_name(ac)), false).toBool();
}

void AC_saveValue(const QString& user, AC ac, bool allowed)
{
    QSettings settings;
    settings.setValue(QString("u_%1/ac_%2").arg(user, AC_name(ac)), allowed);
}

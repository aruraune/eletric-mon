#include "usereditor.h"
#include "ui_usereditor.h"
#include <QCheckBox>
#include <QCryptographicHash>
#include <QSettings>

UserEditor::UserEditor(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::UserEditor),
    m_newUser(false),
    m_passwordUpdated(false)
{
    ui->setupUi(this);
    this->setupSignals();
}

UserEditor::~UserEditor()
{
    ui->in_Password->setText(QString(ui->in_Password->text().length(), '\0'));
    delete ui;
}

QString UserEditor::userName() const
{
    return ui->in_UserName->text();
}

QByteArray UserEditor::password() const
{
    QString salted = this->userName() + ":" + ui->in_Password->text();
    return QCryptographicHash::hash(salted.toUtf8(), QCryptographicHash::Sha3_512);
}

void UserEditor::allow(AC ac, bool allowed)
{
    QList<QCheckBox*> cbs = ui->view_Permissions->findChildren<QCheckBox*>(QString());
    QList<QCheckBox*>::ConstIterator it = cbs.begin();

    for (; it != cbs.end(); ++it)
    {
        if (*it)
        {
            bool success = false;
            AC cbac = static_cast<AC>((*it)->property("ac").toInt(&success));

            if (success && cbac == ac)
            {
                m_permissions[cbac] = allowed;
                (*it)->setChecked(allowed);
            }
        }
    }
}

void UserEditor::accept()
{
    QSettings settings;
    QString user = this->userName();

    bool userUpdated = (!m_newUser && !m_oldUser.isEmpty() && user != m_oldUser);
    bool emptyPassword = ui->in_Password->text().isEmpty();

    if (emptyPassword && (m_passwordUpdated || userUpdated || m_newUser))
    {
        ui->in_Password->setPlaceholderText(tr("Você precisa definir uma nova senha."));
        ui->in_Password->setFocus();
        return;
    }

    if (userUpdated)
    {
        settings.remove(QString("u_%1").arg(m_oldUser));
        m_newUser = true;
    }

    if (m_newUser)
    {
        settings.setValue(QString("u_%1/username").arg(user), user);
    }

    if (m_newUser || m_passwordUpdated)
    {
        settings.setValue(QString("u_%1/password").arg(user), this->password());
    }

    QList<QCheckBox*> cbs = ui->view_Permissions->findChildren<QCheckBox*>(QString());
    QList<QCheckBox*>::ConstIterator it = cbs.begin();

    for (; it != cbs.end(); ++it)
    {
        if (*it)
        {
            bool success = false;
            AC ac = static_cast<AC>((*it)->property("ac").toInt(&success));

            if (success)
            {
                m_permissions[ac] = (*it)->isChecked();
            }
        }
    }

    AC_save(m_permissions, user);

    QDialog::accept();
}

void UserEditor::load(const QString& user, bool forceNewUser)
{
    m_oldUser = user;
    m_newUser = (user.isEmpty() || forceNewUser);

    if (!user.isEmpty())
    {
        ui->in_UserName->setText(user);
    }

    if (!m_newUser)
    {
        ui->in_Password->setPlaceholderText("Senha não alterada.");
    }
    else
    {
        ui->in_Password->setPlaceholderText("Escolha uma senha.");
    }

    if (!m_permissions.isEmpty())
    {
        m_permissions.clear();
    }

    QList<QCheckBox*> cbs = ui->view_Permissions->findChildren<QCheckBox*>(QString(), Qt::FindDirectChildrenOnly);

    if (!cbs.isEmpty())
    {
        while (!cbs.isEmpty())
        {
            QCheckBox* cb = cbs.takeLast();

            if (cb)
            {
                cb->deleteLater();
                cb = 0;
            }
        }
    }

    if (!m_newUser)
    {
        m_permissions = AC_load(user);
    }
    else
    {
        m_permissions = AC_load();
    }

    if (!m_permissions.isEmpty())
    {
        QMap<AC, bool>::Iterator it = m_permissions.begin();

        int maxCol = 3;

        int endCol = 0;
        int endRow = 0;

        for (int row = 0 ;; ++row)
        {
            if (it == m_permissions.end())
            {
                break;
            }

            for (int col = 0; col < maxCol; ++col)
            {
                if (it == m_permissions.end())
                {
                    break;
                }

                AC ac = it.key();
                QCheckBox* cb = new QCheckBox(AC_description(ac), this);

                if (cb)
                {
                    cb->setChecked(it.value());
                    cb->setProperty("ac", static_cast<int>(ac));
                    cb->setObjectName(QString("ac_%1").arg(AC_name(ac)));

                    ui->grid->addWidget(cb, row, col);
                    ui->grid->setAlignment(cb, Qt::AlignLeft | Qt::AlignTop);
                }

                ++it;

                if (col > endCol)
                {
                    endCol = col;
                }
            }

            if (row > endRow)
            {
                endRow = row;
            }
        }

        ui->grid->addWidget(ui->gridSpacer_Horizontal, 0, endCol + 1);
        ui->grid->addWidget(ui->gridSpacer_Vertical, endRow + 1, 0);

        this->adjustSize();
    }
}

void UserEditor::passwordUpdated()
{
    m_passwordUpdated = ui->in_Password->text().isEmpty();
}

void UserEditor::setupSignals()
{
    connect(ui->in_Password, &QLineEdit::textChanged, this, &UserEditor::passwordUpdated);
}

#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>

namespace Ui
{
    class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    SettingsDialog(QWidget* parent = 0);
    ~SettingsDialog();

    double maxPower() const;
    void setMaxPower(double power);

    double initialPower() const;
    void setInitialPower(double power);

    double warningRange() const;
    void setWarningRange(double range);

    double criticalRange() const;
    void setCriticalRange(double range);

public slots:
    void loadSettings();
    void accept();

private:
    Ui::SettingsDialog* ui;
};

#endif // SETTINGSDIALOG_H

#ifndef AC_LIST_H
#define AC_LIST_H

#include <QMap>
#include <QString>

/*
 *  ACL Fields:
 *
 *    LOGIN - Login permission.
 *    MANAGE - Create, edit and remove controllers.
 *    SETTINGS - Edit application settings.
 *    USERS - Create, edit and remove users.
 *    POWER - Turn controller on/off.
 *    ADMIN - Administrator permissions.
 *
 */

enum class AC
{
    LOGIN = 1,
    MANAGE,
    SETTINGS,
    USERS,
    POWER,
    ADMIN
};

QMap<AC, bool> AC_load(const QString& user = "default");
void AC_save(const QMap<AC, bool>& ac, const QString& user = "default");

QString AC_name(AC ac);
QString AC_description(AC ac);

bool AC_loadValue(const QString& user, AC ac);
void AC_saveValue(const QString& user, AC ac, bool allowed);

#endif // AC_LIST_H

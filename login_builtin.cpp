#include "login_builtin.h"
#include <QSettings>
#include <QCryptographicHash>

Login_BuiltIn::Login_BuiltIn(QObject* parent) :
    QObject(parent)
{
}

Login_BuiltIn::~Login_BuiltIn()
{
}

int Login_BuiltIn::authenticate(const QString& name, const QString& password)
{
    QSettings settings;

    QString u_user = settings.value(QString("u_%1/username").arg(name), QString()).toString();
    QByteArray u_hash = settings.value(QString("u_%1/password").arg(name), QByteArray()).toByteArray();

    if (!u_user.isEmpty() && !u_hash.isEmpty() && u_user == name)
    {
        u_user.append(":").append(password);
        QByteArray o_hash = QCryptographicHash::hash(u_user.toUtf8(), QCryptographicHash::Sha3_512);
        u_user = QString(u_user.length(), '\0');

        if (o_hash.isEmpty() || u_hash != o_hash)
        {
            return 0; // Wrong user/password
        }
    }
    else
    {
        return 0; // Wrong user/password
    }

    m_loginDetails.setUserName(name);
    m_authenticated = (this->loadACL() && this->validateACL(AC::LOGIN));

    return (m_authenticated ? 1 : -1);
}

bool Login_BuiltIn::deauthenticate()
{
    return !(m_authenticated = false);
}

bool Login_BuiltIn::isAuthenticated() const
{
    return m_authenticated;
}

QString Login_BuiltIn::description() const
{
    return "BuiltIn (Built-in development login interface)";
}

LoginDetails* Login_BuiltIn::loginDetails()
{
    return &m_loginDetails;
}

const LoginDetails& Login_BuiltIn::loginDetails_Const() const
{
    return m_loginDetails;
}

bool Login_BuiltIn::loadACL()
{
    QString user = m_loginDetails.userName();

    AccessControl* ac = m_loginDetails.accessControl();

    if (ac)
    {
        ac->setAC(AC_load(user));
    }

    return true;
}

bool Login_BuiltIn::validateACL(AC ac) const
{
    bool success = m_loginDetails.accessControl_Const().value(ac);

    // Administrators always have all permissions.
    if (!success && m_loginDetails.accessControl_Const().value(AC::ADMIN))
    {
        success = true;
    }

    return success;
}

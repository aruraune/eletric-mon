#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QTimer>
#include <QUrl>
#include <QContextMenuEvent>
#include "controller.h"
#include "controllereditor.h"
#include "controllerwidget.h"
#include <QSettings>
#include "settingsdialog.h"
#include <QMessageBox>
#include "aboutdialog.h"
#include "userlist.h"

MainWindow::MainWindow(QWidget* parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_login(0),
    m_currentUser("anonymous"),
    m_maxPower(0.0),
    m_initialPower(0.0),
    m_currentPower(0.0),
    m_warningPower(0.0),
    m_criticalPower(0.0)
{
    ui->setupUi(this);

    this->setupMisc();
    this->setupToolbar();
    this->setupSignals();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::authenticationReady(bool success, LoginInterface* interface)
{
    if (!interface || !success)
    {
        QTimer::singleShot(0, this, SLOT(close()));
    }
    else
    {
        m_login = interface;

        if (m_login)
        {
            QTimer::singleShot(0, this, SLOT(show()));
            this->setupAfterLogin();

            m_currentUser = m_login->loginDetails_Const().userName();
        }
    }
}

bool MainWindow::addNewController()
{
    if (m_login)
    {
        if (!m_login->validateACL(AC::MANAGE))
        {
            QMessageBox::information(this, tr("Ação Não Permitida"), tr("Você não possui permissões para adicionar circuitos."));
            return false;
        }
    }
    else
    {
        this->internalError();
        return false;
    }

    bool success = true;

    Controller* controller = new Controller(this);

    if (!controller)
    {
        success = false;
    }

    if (success)
    {
        ControllerEditor* editor = controller->editor();

        success = (editor != 0);

        if (success)
        {
            editor->disableDeletion();

            success = (editor->exec() == ControllerEditor::Accepted);
            controller->freeEditor();

            if (success)
            {
                ControllerWidget* widget = new ControllerWidget(ui->scrollAreaWidgetContents);

                success = (widget != 0);

                if (success)
                {
                    widget->setController(controller);
                    widget->reloadController();

                    this->updateView();

                    this->setupControllerWidget(widget);
                }
            }
        }
    }

    if (success)
    {
        emit controllersUpdated();
    }
    else
    {
        controller->deleteLater();
        controller = 0;
    }

    return success;
}

void MainWindow::updateView()
{
    QList<ControllerWidget*> items = this->controllerWidgets();

    if (!items.isEmpty())
    {
        QList<ControllerWidget*>::Iterator it = items.begin();

        int maxCol = ui->in_Columns->value();

        if (maxCol <= 0)
        {
            maxCol = 3;
            ui->in_Columns->setValue(3);
        }

        int endCol = 0;
        int endRow = 0;

        for (int row = 0 ;; ++row)
        {
            if (it == items.end())
            {
                break;
            }

            for (int col = 0; col < maxCol; ++col)
            {
                if (it == items.end())
                {
                    break;
                }

                if (*it)
                {
                    ui->grid->addWidget(*it, row, col);
                    ui->grid->setAlignment(*it, Qt::AlignLeft | Qt::AlignTop);
                }

                ++it;

                if (col > endCol)
                {
                    endCol = col;
                }
            }

            if (row > endRow)
            {
                endRow = row;
            }
        }

        ui->grid->addWidget(ui->gridSpacer_Horizontal, 0, endCol + 1);
        ui->grid->addWidget(ui->gridSpacer_Vertical, endRow + 1, 0);
    }
}
void MainWindow::saveControllers()
{
    QSettings settings;

    QList<Controller*> controllers = this->controllers();

    if (!controllers.isEmpty())
    {
        QList<Controller*>::Iterator it = controllers.begin();

        settings.beginWriteArray("Controller");
        settings.remove("");

        for (int i = 0; it != controllers.end(); ++it)
        {
            if (*it)
            {
                settings.setArrayIndex(i++);
                settings.setValue("data", (*it)->exportData());
            }
        }

        settings.endArray();
    }

    this->validateCircuits();
}

void MainWindow::loadControllers()
{
    QSettings settings;

    int size = settings.beginReadArray("Controller");

    for (int i = 0; i < size; ++i)
    {
        settings.setArrayIndex(i);

        QByteArray data = settings.value("data").toByteArray();

        if (!data.isEmpty())
        {
            Controller* controller = new Controller(this);

            if (controller)
            {
                if (controller->importData(data))
                {
                    this->addController(controller);
                }
                else
                {
                    controller->deleteLater();
                    controller = 0;
                }
            }
        }
    }

    settings.endArray();

    emit controllersUpdated();

    this->validateCircuits();
}

void MainWindow::loadPreferences()
{
    QSettings settings;
    int columns = settings.value(QString("u_%1/columns").arg(m_currentUser), 3).toInt();
    ui->in_Columns->setValue(columns);
}

void MainWindow::loadSettings()
{
    QSettings settings;

    m_maxPower = settings.value("max-power", 0.0).toDouble();
    m_initialPower = settings.value("initial-power", 0.0).toDouble();

    m_currentPower = m_initialPower;

    m_warningPower = (m_maxPower / 100.0) * settings.value("warning-range", 60.0).toDouble();
    m_criticalPower = (m_maxPower / 100.0) * settings.value("critical-range", 80.0).toDouble();

    this->updatePower();
}

void MainWindow::updatePower()
{
    QList<Controller*> controllers = this->controllers();

    m_currentPower = m_initialPower;

    if (!controllers.isEmpty())
    {
        QList<Controller*>::Iterator it = controllers.begin();

        for (; it != controllers.end(); ++it)
        {
            if (*it && (*it)->isPowered())
            {
                m_currentPower += (*it)->power();

                if (m_currentPower > m_maxPower)
                {
                    (*it)->turnOff();
                    QMessageBox::warning(this, tr("Carga Máxima Excedida"), tr("Não foi possível energizar o circuito %1.\nMotivo: Ligá-lo irá exceder a carga máxima permitida.").arg((*it)->id()));
                }
            }
        }
    }

    if (m_currentPower < m_warningPower && m_currentPower < m_criticalPower)
    {
        ui->txt_Power->setStyleSheet("color: black;");
    }
    else if (m_currentPower >= m_criticalPower)
    {
        ui->txt_Power->setStyleSheet("color: red;");
    }
    else
    {
        ui->txt_Power->setStyleSheet("color: orange;");
    }

    ui->txt_Power->setText(QString("%1/%2 KVA").arg(m_currentPower).arg(m_maxPower));
}

bool MainWindow::addController(Controller* controller)
{
    bool success = false;

    if (controller)
    {
        ControllerWidget* widget = new ControllerWidget(ui->scrollAreaWidgetContents);

        if (widget)
        {
            widget->setController(controller);
            widget->reloadController();

            this->updateView();

            this->setupControllerWidget(widget);

            success = true;
        }
    }

    return success;
}

void MainWindow::setupControllerWidget(ControllerWidget* widget)
{
    if (widget)
    {
        connect(widget, &ControllerWidget::controllerUpdated, this, &MainWindow::saveControllers);
        connect(widget, &ControllerWidget::controllerUpdated, this, &MainWindow::updatePower);

        connect(widget, &ControllerWidget::controllerDeleted, this, &MainWindow::saveControllers);
        connect(widget, &ControllerWidget::controllerDeleted, this, &MainWindow::updateView);

        connect(widget, &ControllerWidget::requestPowerUp, this, &MainWindow::turnControllerOn);
        connect(widget, &ControllerWidget::requestPowerDown, this, &MainWindow::turnControllerOff);

        connect(widget, &ControllerWidget::powerStateChanged, this, &MainWindow::updatePower);

        widget->connectController();
    }
}

bool MainWindow::turnControllerOn()
{
    if (m_login)
    {
        if (!m_login->validateACL(AC::POWER))
        {
            QMessageBox::information(this, tr("Ação Não Permitida"), tr("Você não possui permissões para energizar circuitos."));
            return false;
        }
    }
    else
    {
        this->internalError();
        return false;
    }

    bool success = false;

    ControllerWidget* widget = qobject_cast<ControllerWidget*>(this->sender());

    if (widget)
    {
        Controller* controller = widget->controller();

        if (controller)
        {
            if ((m_currentPower + controller->power()) <= m_maxPower)
            {
                success = widget->turnOn();
            }
            else
            {
                QMessageBox::information(this, tr("Carga Máxima Excedida"), tr("Não foi possível energizar este circuito.\nMotivo: Ligá-lo irá exceder a carga máxima permitida."));
            }
        }
    }

    return success;
}

bool MainWindow::turnControllerOff()
{
    if (m_login)
    {
        if (!m_login->validateACL(AC::POWER))
        {
            QMessageBox::information(this, tr("Ação Não Permitida"), tr("Você não possui permissões para desligar circuitos."));
            return false;
        }
    }
    else
    {
        this->internalError();
        return false;
    }

    bool success = false;

    ControllerWidget* widget = qobject_cast<ControllerWidget*>(this->sender());

    if (widget)
    {
        success = widget->turnOff();
    }

    return success;
}

void MainWindow::setupAfterLogin()
{
    QTimer::singleShot(0, this, SLOT(loadSettings()));
    QTimer::singleShot(0, this, SLOT(loadPreferences()));
    QTimer::singleShot(0, this, SLOT(loadControllers()));
}

void MainWindow::saveColumnCount()
{
    QSettings settings;
    settings.setValue(QString("u_%1/columns").arg(m_currentUser), ui->in_Columns->value());
}

void MainWindow::openSettings()
{
    if (m_login)
    {
        if (!m_login->validateACL(AC::SETTINGS))
        {
            QMessageBox::information(this, tr("Ação Não Permitida"), tr("Você não possui permissões para editar as configurações."));
            return;
        }
    }
    else
    {
        this->internalError();
        return;
    }

    QList<ControllerWidget*> controllers = this->controllerWidgets();

    if (!controllers.isEmpty())
    {
        QList<ControllerWidget*>::Iterator it = controllers.begin();

        for (; it != controllers.end(); ++it)
        {
            if (*it && (*it)->isPowered())
            {
                QMessageBox::information(this, tr("Ação Não Permitida"), tr("As configuraçõe não devem ser editadas enquanto houverem circuitos energizados."));
                return;
            }
        }
    }

    SettingsDialog dialog(this);

    if (dialog.exec() == SettingsDialog::Accepted)
    {
        this->loadSettings();

        if (!controllers.isEmpty())
        {
            QList<ControllerWidget*>::Iterator it = controllers.begin();

            for (; it != controllers.end(); ++it)
            {
                if (*it && (*it)->isPowered())
                {
                    (*it)->turnOff();
                }
            }
        }
    }
}

void MainWindow::openUsers()
{
    if (m_login)
    {
        if (!m_login->validateACL(AC::USERS))
        {
            QMessageBox::information(this, tr("Ação Não Permitida"), tr("Você não possui permissões para editar usuários."));
            return;
        }
    }
    else
    {
        this->internalError();
        return;
    }

    UserList users(this);
    users.exec();
}

void MainWindow::about()
{
    AboutDialog ad(this);
    ad.exec();
}

void MainWindow::aboutQt()
{
    QApplication::aboutQt();
}

bool MainWindow::validateCircuits()
{
    QList<Controller*> controllers = this->controllers();
    QList<Controller*>::Iterator it = controllers.begin();

    // server 1-1 port 1-1 pin 1-N client
    QMap<quint32, QMap<quint16, QMultiMap<quint8, Controller*>>> server;

    QStringList invalid;

    for (; it != controllers.end(); ++it)
    {
        if (*it)
        {
            quint32 ip = (*it)->ip();
            quint16 port = (*it)->port();
            quint8 pin = (*it)->pin();

            server[ip][port].insertMulti(pin, *it);
        }
    }

    QMap<quint32, QMap<quint16, QMultiMap<quint8, Controller*>>>::Iterator it_server = server.begin();

    for (; it_server != server.end(); ++it_server)
    {
        quint32 ip = it_server.key();
        QMap<quint16, QMultiMap<quint8, Controller*>>& instances = it_server.value();
        QMap<quint16, QMultiMap<quint8, Controller*>>::Iterator it_instance = instances.begin();

        for (; it_instance != instances.end(); ++it_instance)
        {
            quint16 port = it_instance.key();
            QMultiMap<quint8, Controller*>& connections = it_instance.value();

            QList<quint8> pins = connections.uniqueKeys();
            QList<quint8>::Iterator it_pin = pins.begin();

            for (; it_pin != pins.end(); ++it_pin)
            {
                quint8 pin = *it_pin;

                if (connections.count(pin) > 1)
                {
                    QList<Controller*> clients = connections.values(pin);
                    QList<Controller*>::Iterator it_client = clients.begin();

                    for (; it_client != clients.end(); ++it_client)
                    {
                        Controller* client = (*it_client);

                        if (client)
                        {
                            if (!client->duplicate())
                            {
                                if (client->isPowered())
                                {
                                    client->turnOff();
                                }

                                client->setDuplicate(true);
                                invalid << tr("%1: %2:%3 pino %4").arg(client->id()).arg(QHostAddress(ip).toString()).arg(port).arg(pin);
                            }
                        }
                    }
                }
                else
                {
                    Controller* client = connections.value(pin);

                    if (client && client->duplicate())
                    {
                        client->setDuplicate(false);
                    }
                }
            }
        }
    }

    if (!invalid.isEmpty())
    {
        QMessageBox::warning(this, tr("Configurações inválidas"), tr("Os seguintes circuitos possuem configurações de rede inválidas:\n- ").append(invalid.join("\n- ")));
    }

    return invalid.isEmpty();
}

void MainWindow::setupMisc()
{
    ui->grid->setAlignment(ui->gridSpacer_Horizontal, Qt::AlignLeft | Qt::AlignTop);
    ui->grid->setAlignment(ui->gridSpacer_Vertical, Qt::AlignLeft | Qt::AlignTop);

    qApp->setWindowIcon(QIcon(":/icon/icon/server_lightning.png"));
}

void MainWindow::setupToolbar()
{
    ui->toolBar->addAction(ui->actionNewController);
    ui->toolBar->addAction(ui->actionSettings);
    ui->toolBar->addAction(ui->actionUsers);
    ui->toolBar->addAction(ui->actionQuit);
}

void MainWindow::setupSignals()
{
    connect(ui->actionNewController, &QAction::triggered, this, &MainWindow::addNewController);
    connect(ui->actionSettings, &QAction::triggered, this, &MainWindow::openSettings);
    connect(ui->actionUsers, &QAction::triggered, this, &MainWindow::openUsers);
    connect(ui->actionAbout, &QAction::triggered, this, &MainWindow::about);
    connect(ui->actionAboutQt, &QAction::triggered, this, &MainWindow::aboutQt);

    connect(ui->in_Columns, SIGNAL(valueChanged(int)), this, SLOT(updateView()));
    connect(ui->in_Columns, SIGNAL(valueChanged(int)), this, SLOT(saveColumnCount()));

    connect(this, &MainWindow::controllersUpdated, this, &MainWindow::saveControllers);
    connect(this, &MainWindow::controllersUpdated, this, &MainWindow::updatePower);
}

void MainWindow::internalError()
{
    QMessageBox::critical(this, tr("Erro Interno"), tr("Não foi possível carregar um dos componentes internos necessários."));
    this->close();
}

QList<ControllerWidget*> MainWindow::controllerWidgets()
{
    return ui->scrollAreaWidgetContents->findChildren<ControllerWidget*>(QString(), Qt::FindDirectChildrenOnly);
}

QList<Controller*> MainWindow::controllers()
{
    QList<Controller*> controllers;
    QList<ControllerWidget*> widgets = this->controllerWidgets();
    QList<ControllerWidget*>::Iterator it = widgets.begin();

    for (; it != widgets.end(); ++it)
    {
        ControllerWidget* widget = qobject_cast<ControllerWidget*>(*it);

        if (widget)
        {
            Controller* controller = widget->controller();

            if (controller)
            {
                controllers.append(controller);
            }
        }
    }

    return controllers;
}

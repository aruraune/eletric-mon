#include "userlist.h"
#include "ui_userlist.h"
#include <QSettings>
#include "usereditor.h"

UserList::UserList(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::UserList)
{
    ui->setupUi(this);
    this->setupSignals();

    this->loadUsers();
}

UserList::~UserList()
{
    delete ui;
}

void UserList::addUser()
{
    UserEditor editor(this);
    editor.load();

    if (editor.exec() == UserEditor::Accepted)
    {
        this->loadUsers();
    }
}

void UserList::editUser()
{
    QListWidgetItem* item = ui->view_Users->currentItem();

    if (item)
    {
        QString user = item->text();

        if (!user.isEmpty())
        {
            UserEditor editor(this);
            editor.load(item->text(), false);

            if (editor.exec() == UserEditor::Accepted)
            {
                this->loadUsers();
            }
        }
    }
}

void UserList::removeUser()
{
    if (ui->view_Users->count() == 1)
    {
        QListWidgetItem* item = ui->view_Users->currentItem();

        if (item)
        {
            QString user = item->text();

            if (!user.isEmpty())
            {
                QSettings settings;
                settings.remove(user.prepend("u_"));

                this->loadUsers();
            }
        }
    }
}

void UserList::loadUsers()
{
    QSettings settings;

    ui->view_Users->clear();

    QStringList u_users = settings.allKeys().filter(QRegExp("^u_[^/]+/username$")).replaceInStrings(QRegExp("/username$"), QString());
    u_users.removeDuplicates();

    ui->view_Users->addItems(u_users.replaceInStrings(QRegExp("^u_"), QString()));

    this->enableEditButton();
}

void UserList::enableEditButton()
{
    ui->btn_Edit->setDisabled(ui->view_Users->currentItem() == 0);
}

void UserList::setupSignals()
{
    connect(ui->btn_Add, &QPushButton::clicked, this, &UserList::addUser);
    connect(ui->btn_Edit, &QPushButton::clicked, this, &UserList::editUser);
    connect(ui->btn_Del, &QPushButton::clicked, this, &UserList::removeUser);
    connect(ui->view_Users, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)), this, SLOT(enableEditButton()));
}

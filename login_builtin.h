#ifndef LOGIN_BUILTIN_H
#define LOGIN_BUILTIN_H

#include <QObject>
#include "logininterface.h"

class Login_BuiltIn : public QObject, public LoginInterface
{
    Q_OBJECT

public:
    Login_BuiltIn(QObject* parent);
    virtual ~Login_BuiltIn();

    int authenticate(const QString& name, const QString& password);
    bool deauthenticate();

    bool isAuthenticated() const;

    QString description() const;

    LoginDetails* loginDetails();
    const LoginDetails& loginDetails_Const() const;

    bool loadACL();
    bool validateACL(AC ac) const;

private:
    bool m_authenticated;
    LoginDetails m_loginDetails;
};

#endif // LOGIN_BUILTIN_H

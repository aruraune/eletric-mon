#ifndef CONTROLLER_STATE_H
#define CONTROLLER_STATE_H

enum class ControllerState
{
    Invalid,
    Updating,
    Powered,
    Unpowered
};

#endif // CONTROLLER_STATE_H

#include "logindetails.h"

LoginDetails::LoginDetails()
{
}

LoginDetails::~LoginDetails()
{
}

void LoginDetails::setUserName(const QString& name)
{
    m_userName = name;
}

QString LoginDetails::userName() const
{
    return m_userName;
}

AccessControl* LoginDetails::accessControl()
{
    return &m_acl;
}

const AccessControl& LoginDetails::accessControl_Const() const
{
    return m_acl;
}

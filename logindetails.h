#ifndef LOGININFO_H
#define LOGININFO_H

#include <QString>
#include <QStringList>
#include "accesscontrol.h"

class LoginDetails
{
public:
    LoginDetails();
    virtual ~LoginDetails();

    void setUserName(const QString& name);
    QString userName() const;

    AccessControl* accessControl();
    const AccessControl& accessControl_Const() const;

private:
    QString m_userName;
    AccessControl m_acl;
};

#endif // LOGININFO_H

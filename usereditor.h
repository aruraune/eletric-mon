#ifndef USEREDITOR_H
#define USEREDITOR_H

#include <QDialog>
#include "ac_fields.h"

namespace Ui
{
    class UserEditor;
}

class UserEditor : public QDialog
{
    Q_OBJECT

public:
    UserEditor(QWidget* parent = 0);
    ~UserEditor();

    QString userName() const;
    QByteArray password() const;

    void allow(AC ac, bool allowed = true);

public slots:
    void accept();
    void load(const QString& user = QString(), bool forceNewUser = false);

private slots:
    void passwordUpdated();

private:
    void setupSignals();

private:
    Ui::UserEditor* ui;
    QMap<AC, bool> m_permissions;

    bool m_newUser;
    QString m_oldUser;

    bool m_passwordUpdated;
};

#endif // USEREDITOR_H

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "logininterface.h"

namespace Ui
{
    class MainWindow;
}

class Controller;
class ControllerWidget;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget* parent = 0);
    ~MainWindow();

public slots:
    void authenticationReady(bool success, LoginInterface* interface);
    bool addNewController();
    void updateView();

    void saveControllers();
    void loadControllers();
    void loadPreferences();
    void loadSettings();

    void updatePower();

    void openSettings();
    void openUsers();
    void about();
    void aboutQt();

    bool validateCircuits();

signals:
    void controllersUpdated();

private slots:
    bool turnControllerOn();
    bool turnControllerOff();

    void setupAfterLogin();
    void saveColumnCount();

private:
    bool addController(Controller* controller);
    void setupControllerWidget(ControllerWidget* widget);
    void setupMisc();
    void setupToolbar();
    void setupSignals();

    void internalError();

    QList<ControllerWidget*> controllerWidgets();
    QList<Controller*> controllers();

private:
    Ui::MainWindow* ui;
    LoginInterface* m_login;

    QString m_currentUser;

    double m_maxPower;
    double m_initialPower;
    double m_currentPower;
    double m_warningPower;
    double m_criticalPower;
};

#endif // MAINWINDOW_H

#ifndef LOGININTERFACE_H
#define LOGININTERFACE_H

#include "logindetails.h"
#include "ac_fields.h"

class LoginInterface
{
public:
    virtual int authenticate(const QString& name, const QString& password) = 0;
    // return: 1=Ok, 0=Wrong user/password, -1=Access denied

    virtual bool deauthenticate() = 0;

    virtual bool isAuthenticated() const = 0;

    virtual QString description() const = 0;

    virtual LoginDetails* loginDetails() = 0;
    virtual const LoginDetails& loginDetails_Const() const = 0;

    virtual bool loadACL() = 0;
    virtual bool validateACL(AC ac) const = 0;
};

#endif // LOGININTERFACE_H

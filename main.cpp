#include "mainwindow.h"
#include <QApplication>
#include "logindialog.h"
#include "login_builtin.h"
#include <QMessageBox>
#include <QSettings>
#include <QStringList>
#include "usereditor.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setOrganizationName("Forest");
    a.setOrganizationDomain("tree@forest");
    a.setApplicationName("Gerenciador de Cargas");

    QSettings settings;

    QStringList admins = settings.allKeys().filter(QRegExp("^u_[^/]+/ac_admin$"));
    admins.removeDuplicates();

    while (!admins.isEmpty())
    {
        QString admin = admins.first();

        if (admin.isEmpty() || !settings.value(admin, false).toBool())
        {
            admins.removeFirst();
            continue;
        }

        break;
    }

    if (admins.isEmpty())
    {
        int ret = QMessageBox::question(0, QObject::tr("Nenhum Administrador"), QObject::tr("Não há administradores cadastrados.\nDeseja cadastrar um administrador agora?"));

        if (ret == QMessageBox::Yes)
        {
            UserEditor users;
            users.load("admin", true);
            users.allow(AC::ADMIN);
            users.exec();
        }
        else
        {
            return 0;
        }
    }

    LoginDialog loginDialog;
    LoginInterface* loginInterface = new Login_BuiltIn(&loginDialog);

    if (!loginInterface)
    {
        QMessageBox::critical(0, a.tr("Erro Fatal"), a.tr("Não foi possível instancializar um dos componentes internos necessários."));
        return -1;
    }

    loginDialog.setLoginInterface(loginInterface);

    int returnCode = 0;

    MainWindow mainWindow;

    QObject::connect(&loginDialog, &LoginDialog::authenticationFinished,
                     &mainWindow, &MainWindow::authenticationReady);

    if (loginDialog.exec() == LoginDialog::Accepted &&
        loginInterface->isAuthenticated())
    {
        returnCode = a.exec();
    }

    return returnCode;
}

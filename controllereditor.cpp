#include "controllereditor.h"
#include "ui_controllereditor.h"
#include "controller.h"
#include <QFileInfo>
#include <QFileDialog>
#include <QMessageBox>

ControllerEditor::ControllerEditor(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::ControllerEditor),
    m_controller(0),
    m_deleteController(false)
{
    ui->setupUi(this);
    this->setupSignals();

    this->disableDeletion();
}

ControllerEditor::~ControllerEditor()
{
    delete ui;
}

Controller* ControllerEditor::controller() const
{
    return m_controller;
}

void ControllerEditor::setController(Controller* controller)
{
    m_controller = controller;
}

QString ControllerEditor::id() const
{
    return ui->in_Id->text();
}

void ControllerEditor::setId(const QString& id)
{
    ui->in_Id->setText(id);
}

QString ControllerEditor::image() const
{
    return ui->in_Image->text();
}

void ControllerEditor::setImage(const QString& image)
{
    ui->in_Image->setText(image);
}

QString ControllerEditor::description() const
{
    return ui->in_Description->text();
}

void ControllerEditor::setDescription(const QString& description)
{
    ui->in_Description->setText(description);
}

double ControllerEditor::power() const
{
    return ui->in_Power->value();
}

void ControllerEditor::setPower(double power)
{
    ui->in_Power->setValue(power);
}

quint32 ControllerEditor::ip() const
{
    return QHostAddress(ui->in_Server->text()).toIPv4Address();
}

void ControllerEditor::setIp(quint32 ip)
{
    ui->in_Server->setText(QHostAddress(ip).toString());
}

quint16 ControllerEditor::port() const
{
    return ui->in_Port->value();
}

void ControllerEditor::setPort(quint16 port)
{
    ui->in_Port->setValue(port);
}

quint8 ControllerEditor::pin() const
{
    return ui->in_Pin->value();
}

void ControllerEditor::setPin(quint8 pin)
{
    ui->in_Pin->setValue(pin);
}

void ControllerEditor::enableDeletion(bool enabled)
{
    ui->btn_Delete->setEnabled(enabled);
}

void ControllerEditor::disableDeletion(bool disabled)
{
    ui->btn_Delete->setDisabled(disabled);
}

bool ControllerEditor::reloadController()
{
    bool reloaded = false;

    if (m_controller)
    {
        ui->in_Id->setText(m_controller->id());
        ui->in_Image->setText(m_controller->image());
        ui->in_Description->setText(m_controller->description());
        ui->in_Power->setValue(m_controller->power());
        ui->in_Server->setText(QHostAddress(m_controller->ip()).toString());
        ui->in_Port->setValue(m_controller->port());
        ui->in_Pin->setValue(m_controller->pin());

        reloaded = true;
    }

    return reloaded;
}

void ControllerEditor::accept()
{
    if (ui->in_Id->text().isEmpty() ||
        ui->in_Image->text().isEmpty() ||
        ui->in_Description->text().isEmpty() ||
        ui->in_Power->value() < 0 ||
        ui->in_Server->text().isEmpty() ||
        ui->in_Port->value() < 0 ||
        ui->in_Pin->value() < 0)
    {
        QMessageBox::warning(this, tr("Erro"), tr("Preencha os dados corretamente."));
        return;
    }

    if (m_controller)
    {
        m_controller->setId(this->id());
        m_controller->setImage(this->image());
        m_controller->setDescription(this->description());
        m_controller->setPower(this->power());
        m_controller->setIp(QHostAddress(ui->in_Server->text()).toIPv4Address());
        m_controller->setPort(ui->in_Port->value());
        m_controller->setPin(ui->in_Pin->value());
    }

    QDialog::accept();
}

void ControllerEditor::getImage()
{
    QString image = ui->in_Image->text();

    image = QFileDialog::getOpenFileName(qobject_cast<QWidget*>(this), tr("Escolher imagem"), image);

    if (!image.isEmpty())
    {
        ui->in_Image->setText(image);
    }
}

void ControllerEditor::updatePreview()
{
    ui->txt_Preview->setPixmap(QPixmap(ui->in_Image->text()));
}

void ControllerEditor::deleteController()
{
    m_deleteController = true;
    this->done(DeleteController);
}

void ControllerEditor::setupSignals()
{
    connect(ui->btn_Image, &QPushButton::clicked, this, &ControllerEditor::getImage);
    connect(ui->in_Image, &QLineEdit::textChanged, this, &ControllerEditor::updatePreview);
    connect(ui->btn_Delete, &QPushButton::clicked, this, &ControllerEditor::deleteController);
}

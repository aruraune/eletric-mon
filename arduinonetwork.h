#ifndef ARDUINONETWORK_H
#define ARDUINONETWORK_H

#include <QObject>
#include <QHostAddress>

class ArduinoNetwork : public QObject
{
    Q_OBJECT

public:
    ArduinoNetwork(QObject* parent = 0);
    ~ArduinoNetwork();

public slots:
    bool requestPinState(const QHostAddress& address, quint16 port, quint8 pin);
    bool updatePinPower(const QHostAddress& address, quint16 port, quint8, bool power);

private slots:
    void pinStateConnected();
    void updatePowerConnected();
    void socketError(QAbstractSocket::SocketError error);
    void socketDataReceived();
    void socketDisconnected();

signals:
    void pinStateResponse(quint8 pin, bool state);
    void socketFailed(quint8 pin);
};

#endif // ARDUINONETWORK_H

#include "controllerwidget.h"
#include "ui_controllerwidget.h"
#include "controller.h"
#include "controllereditor.h"
#include <QTimer>
#include <QMessageBox>

ControllerWidget::ControllerWidget(QWidget* parent) :
    QWidget(parent),
    ui(new Ui::ControllerWidget),
    m_controller(0)
{
    ui->setupUi(this);
    this->setupMisc();
    this->setupTimers();
    this->setupSignals();
    this->reloadController();
}

ControllerWidget::~ControllerWidget()
{
    delete ui;
}

Controller* ControllerWidget::controller() const
{
    return m_controller;
}

void ControllerWidget::setController(Controller* controller)
{
    m_controller = controller;
}

bool ControllerWidget::connectController()
{
    bool success = false;

    if (m_controller)
    {
        connect(m_controller, &Controller::messageUpdated, this, &ControllerWidget::updateMessage);
        success = connect(m_controller, &Controller::stateUpdated, this, &ControllerWidget::updateState);
    }

    return success;
}

bool ControllerWidget::disconnectController()
{
    bool success = false;

    if (m_controller)
    {
        disconnect(m_controller, &Controller::messageUpdated, this, &ControllerWidget::updateMessage);
        success = disconnect(m_controller, &Controller::stateUpdated, this, &ControllerWidget::updateState);
    }

    return success;
}

bool ControllerWidget::isPowered() const
{
    return (m_controller && m_controller->isPowered());
}

bool ControllerWidget::reloadController()
{
    bool reloaded = false;

    if (m_controller)
    {
        ui->view_Image->setPixmap(QPixmap(m_controller->image()));
        ui->txt_Id->setText(m_controller->id());
        ui->txt_Description->setText(m_controller->description());
        ui->txt_Power->setText(QString("%1 KVA").arg(m_controller->power()));

        m_controller->syncPinState();

        reloaded = true;
    }

    if (reloaded)
    {
        this->updateState();
    }

    return reloaded;
}

void ControllerWidget::editController()
{
    if (m_controller)
    {
        if (!m_controller->isPowered())
        {
            ControllerEditor* editor = m_controller->editor();

            if (editor)
            {
                editor->enableDeletion();

                int ret = editor->exec();

                if (ret == ControllerEditor::Accepted)
                {
                    this->reloadController();

                    emit controllerUpdated();
                }
                else if (ret == DeleteController)
                {
                    m_controller->deleteLater();
                    m_controller = 0;

                    this->deleteLater();

                    emit controllerDeleted();
                }
            }
        }
        else
        {
            QMessageBox::information(this, tr("Ação Não Permitida"), tr("As informações do circuito não devem ser alteradas enquanto o mesmo está energizado."));
        }
    }
}

bool ControllerWidget::turnOn()
{
    bool success = false;

    if (m_controller)
    {
        success = m_controller->turnOn();
    }

    return success;
}

bool ControllerWidget::turnOff()
{
    bool success = false;

    if (m_controller)
    {
        success = m_controller->turnOff();
    }

    return success;
}

void ControllerWidget::updatePowerState()
{
    if (m_controller)
    {
        if (m_controller->isValid())
        {
            ui->btn_On->setDisabled(m_controller->isPowered());
            ui->btn_Off->setEnabled(m_controller->isPowered());
        }
        else if (m_controller->state() != ControllerState::Updating)
        {
            ui->btn_On->setDisabled(true);
            ui->btn_Off->setDisabled(true);
        }
    }

    emit powerStateChanged();
}

void ControllerWidget::showStatus()
{
    if (!m_message.isEmpty())
    {
        QMessageBox::information(this, ui->txt_Id->text(), m_message);
    }
}

void ControllerWidget::beginPowerUp()
{
    m_powerUpTimeout.start();
    m_powerUpTimer.start();
}

void ControllerWidget::endPowerUp()
{
    if (ui->btn_On->isDown())
    {
        ui->btn_On->setDown(false);
    }

    if (m_powerUpTimeout.isValid() && m_powerUpTimeout.hasExpired(1000))
    {
        this->powerUp();
    }

    m_powerUpTimeout.invalidate();
}

void ControllerWidget::powerUp()
{
    emit requestPowerUp();
}

void ControllerWidget::beginPowerDown()
{
    m_powerDownTimeout.start();
    m_powerDownTimer.start();
}

void ControllerWidget::endPowerDown()
{
    if (ui->btn_Off->isDown())
    {
        ui->btn_Off->setDown(false);
    }

    if (m_powerDownTimeout.isValid() && m_powerDownTimeout.hasExpired(1000))
    {
        this->powerDown();
    }

    m_powerDownTimeout.invalidate();
}

void ControllerWidget::powerDown()
{
    emit requestPowerDown();
}

void ControllerWidget::updateState()
{
    this->updatePowerState();

    ControllerState state = ControllerState::Invalid;

    if (m_controller)
    {
        state = m_controller->state();
    }

    QString style;

    switch (state)
    {
        case ControllerState::Updating:
            style = "background-color: rgb(255, 255, 127);";
            break;

        case ControllerState::Powered:
            style = "background-color: rgb(85, 255, 127);";
            break;

        case ControllerState::Unpowered:
            style = "background-color: rgb(184, 0, 0);";
            break;

        default:
            style = "background-color: rgb(100, 100, 100);";
            break;
    }

    ui->btn_State->setStyleSheet(style);
}

void ControllerWidget::updateMessage(const QString& message)
{
    m_message = message;
}

void ControllerWidget::setupTimers()
{
    m_powerUpTimer.setSingleShot(true);
    m_powerUpTimer.setInterval(1000 + 100);

    m_powerDownTimer.setSingleShot(true);
    m_powerDownTimer.setInterval(1000 + 100);
}

void ControllerWidget::setupSignals()
{
    connect(ui->btn_Edit, &QToolButton::clicked, this, &ControllerWidget::editController);

    connect(ui->btn_On, &QToolButton::pressed, this, &ControllerWidget::beginPowerUp);
    connect(ui->btn_On, &QToolButton::released, this, &ControllerWidget::endPowerUp);

    connect(ui->btn_Off, &QToolButton::pressed, this, &ControllerWidget::beginPowerDown);
    connect(ui->btn_Off, &QToolButton::released, this, &ControllerWidget::endPowerDown);

    connect(&m_powerUpTimer, &QTimer::timeout, this, &ControllerWidget::endPowerUp);
    connect(&m_powerDownTimer, &QTimer::timeout, this, &ControllerWidget::endPowerDown);

    connect(ui->btn_State, &QToolButton::clicked, this, &ControllerWidget::showStatus);
}

void ControllerWidget::setupMisc()
{
    QPalette palette = this->palette();
    QColor color = palette.color(this->backgroundRole());
    palette.setColor(this->backgroundRole(), color.lighter(110));
    this->setPalette(palette);

    m_message = tr("Não sincronizado.");
}

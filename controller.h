#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include "arduinonetwork.h"
#include "controller_state.h"
#include <QTimer>
#include <QElapsedTimer>

class ControllerEditor;

class Controller : public QObject
{
    Q_OBJECT

public:
    Controller(QObject* parent = 0);
    virtual ~Controller();

    virtual QString id() const;
    virtual void setId(const QString& id);

    virtual QString image() const;
    virtual void setImage(const QString& image);

    virtual QString description() const;
    virtual void setDescription(const QString& description);

    virtual double power() const;
    virtual void setPower(double power);

    virtual quint32 ip() const;
    virtual void setIp(quint32 ip);

    virtual quint16 port() const;
    virtual void setPort(quint16 port);

    virtual quint8 pin() const;
    virtual void setPin(quint8 pin);

    virtual bool duplicate() const;
    virtual void setDuplicate(bool duplicate);

    virtual ControllerEditor* editor();
    virtual void setEditor(ControllerEditor* editor, bool deleteOld = true);
    virtual void freeEditor();

    virtual QByteArray exportData() const;
    virtual bool importData(const QByteArray& data);

    virtual void exportDataToStream(QDataStream& stream) const;
    virtual bool importDataFromStream(QDataStream& stream);

    virtual bool isPowered() const;
    virtual bool isValid() const;

    virtual ControllerState state() const;

public slots:
    virtual void syncPinState();

    virtual bool turnOn();
    virtual bool turnOff();

protected slots:
    virtual void pinState(quint8 pin, bool state);
    virtual void syncError(quint8 pin);

signals:
    void stateUpdated(ControllerState state);
    void messageUpdated(const QString& message);

protected:
    virtual void setupSignals();

private:
    QString m_id;
    QString m_image;
    QString m_description;

    double m_power;

    ControllerEditor* m_editor;
    bool m_deleteEditor;

    ControllerState m_state;
    bool m_powered;

    quint32 m_ip;
    quint16 m_port;
    quint8 m_pin;

    ArduinoNetwork m_arduino;

    QTimer m_sync;
    QElapsedTimer m_syncTimeout;

    bool m_duplicate;

    uint m_errorCount;
    uint m_errorThreshold;
};

#endif // CONTROLLER_H

#ifndef ACCESSCONTROL_H
#define ACCESSCONTROL_H

#include <QMap>
#include "ac_fields.h"

class AccessControl
{
public:
    AccessControl();
    ~AccessControl();

    void allow(AC ac);
    void deny(AC ac);

    bool value(AC ac) const;

    void setAC(const QMap<AC, bool>& ac);

    void clear();

private:
    QMap<AC, bool> m_ac;
};

#endif // ACCESSCONTROL_H

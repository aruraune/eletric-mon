#-------------------------------------------------
#
# Project created by QtCreator 2013-12-09T08:59:01
#
#-------------------------------------------------

QT += core gui declarative network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = eletric-mon
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    logindialog.cpp \
    login_builtin.cpp \
    logindetails.cpp \
    accesscontrol.cpp \
    controller.cpp \
    controllerwidget.cpp \
    controllereditor.cpp \
    settingsdialog.cpp \
    aboutdialog.cpp \
    userlist.cpp \
    ac_fields.cpp \
    usereditor.cpp \
    arduinonetwork.cpp

HEADERS  += mainwindow.h \
    logindialog.h \
    logininterface.h \
    login_builtin.h \
    logindetails.h \
    accesscontrol.h \
    controller.h \
    controllerwidget.h \
    controllereditor.h \
    data_version.h \
    settingsdialog.h \
    aboutdialog.h \
    userlist.h \
    ac_fields.h \
    usereditor.h \
    arduinonetwork.h \
    arduino_protocol.h \
    controller_state.h

FORMS    += mainwindow.ui \
    logindialog.ui \
    controllerwidget.ui \
    controllereditor.ui \
    settingsdialog.ui \
    aboutdialog.ui \
    userlist.ui \
    usereditor.ui

OTHER_FILES += \
    qml/controller.qml \
    qml/control.qml \
    controller/controller.js

RESOURCES += \
    resource.qrc

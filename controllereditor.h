#ifndef CONTROLLEREDITOR_H
#define CONTROLLEREDITOR_H

#include <QDialog>

namespace Ui
{
    class ControllerEditor;
}

class Controller;

class ControllerEditor : public QDialog
{
    Q_OBJECT

public:
    ControllerEditor(QWidget* parent = 0);
    virtual ~ControllerEditor();

    virtual Controller* controller() const;
    virtual void setController(Controller* controller);

    virtual QString id() const;
    virtual void setId(const QString& id);

    virtual QString image() const;
    virtual void setImage(const QString& image);

    virtual QString description() const;
    virtual void setDescription(const QString& description);

    virtual double power() const;
    virtual void setPower(double power);

    virtual quint32 ip() const;
    virtual void setIp(quint32 ip);

    virtual quint16 port() const;
    virtual void setPort(quint16 port);

    virtual quint8 pin() const;
    virtual void setPin(quint8 pin);

    virtual void enableDeletion(bool enabled = true);
    virtual void disableDeletion(bool disabled = true);

    // Parameters

public slots:
    virtual bool reloadController();
    virtual void accept();
    virtual void getImage();
    virtual void updatePreview();
    virtual void deleteController();

private:
    void setupSignals();

private:
    Ui::ControllerEditor* ui;
    Controller* m_controller;
    bool m_deleteController;
};

static const int DeleteController = 42;

#endif // CONTROLLEREDITOR_H

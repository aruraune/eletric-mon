#ifndef USERLIST_H
#define USERLIST_H

#include <QDialog>

namespace Ui
{
    class UserList;
}

class UserList : public QDialog
{
    Q_OBJECT
    
public:
    UserList(QWidget* parent = 0);
    ~UserList();
    
public slots:
    void addUser();
    void editUser();
    void removeUser();
    void loadUsers();

private slots:
    void enableEditButton();

private:
    void setupSignals();

private:
    Ui::UserList* ui;
};

#endif // USERLIST_H

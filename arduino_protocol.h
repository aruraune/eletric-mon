#ifndef ARDUINO_PROTOCOL_H
#define ARDUINO_PROTOCOL_H

enum class Protocol : unsigned short
{
    PortStatus = 1,
    PortPower = 2
};

#endif // ARDUINO_PROTOCOL_H

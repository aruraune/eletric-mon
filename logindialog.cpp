#include "logindialog.h"
#include "ui_logindialog.h"

LoginDialog::LoginDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog),
    m_loginInterface(0)
{
    ui->setupUi(this);
    this->setupSignals();
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

void LoginDialog::setLoginInterface(LoginInterface* interface)
{
    m_loginInterface = interface;
}

LoginInterface* LoginDialog::loginInterface() const
{
    return m_loginInterface;
}

bool LoginDialog::tryAuthenticate()
{
    bool success = false;

    if (!ui->in_User->text().isEmpty())
    {
        if (m_loginInterface != 0)
        {
            int code = m_loginInterface->authenticate(ui->in_User->text(), ui->in_Password->text());

            success = (code == 1);

            if (success)
            {
                this->showInfo(tr("Login aceito."));

                emit authenticationFinished(success, m_loginInterface);
                this->accept();
            }
            else
            {
                if (code == -1)
                {
                    this->showError(tr("Acesso negado."));
                }
                else
                {
                    this->showError(tr("Usuário ou senha incorretos."));
                }
            }
        }
        else
        {
            this->showError(tr("Nenhuma interface de login definida."));
        }
    }
    else
    {
        this->showError(tr("O campo Usuário deve ser preenchido."));
    }

    return success;
}

int LoginDialog::exec()
{
    this->showInfo(tr("Aguardando login..."));

    return QDialog::exec();
}

void LoginDialog::setupSignals()
{
    connect(ui->btn_Cancel, &QPushButton::clicked, this, &LoginDialog::reject);
    connect(ui->btn_Login, &QPushButton::clicked, this, &LoginDialog::tryAuthenticate);
}

void LoginDialog::showInfo(const QString& message)
{
    ui->txt_StatusInfo->setStyleSheet("color: darkgray;");
    ui->txt_StatusInfo->setText(message);
}

void LoginDialog::showError(const QString& message)
{
    ui->txt_StatusInfo->setStyleSheet("color: red;");
    ui->txt_StatusInfo->setText(message);
}

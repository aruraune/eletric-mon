#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>
#include "logininterface.h"

namespace Ui
{
    class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT

public:
    LoginDialog(QWidget* parent = 0);
    ~LoginDialog();

    void setLoginInterface(LoginInterface* interface);
    LoginInterface* loginInterface() const;

public slots:
    bool tryAuthenticate();
    int exec();

signals:
    void authenticationFinished(bool success, LoginInterface* interface);

private:
    void setupSignals();

    void showInfo(const QString& message);
    void showError(const QString& message);

private:
    Ui::LoginDialog* ui;
    LoginInterface* m_loginInterface;
};

#endif // LOGINDIALOG_H

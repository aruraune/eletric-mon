#include "accesscontrol.h"

AccessControl::AccessControl()
{
    this->clear();
}

AccessControl::~AccessControl()
{
}

void AccessControl::allow(AC ac)
{
    m_ac[ac] = true;
}

void AccessControl::deny(AC ac)
{
    m_ac[ac] = false;
}

bool AccessControl::value(AC ac) const
{
    return m_ac.value(ac, false);
}

void AccessControl::setAC(const QMap<AC, bool>& ac)
{
    m_ac = ac;
}

void AccessControl::clear()
{
    m_ac = AC_load();
}

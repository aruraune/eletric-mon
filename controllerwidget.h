#ifndef CONTROLLERWIDGET_H
#define CONTROLLERWIDGET_H

#include <QWidget>
#include <QTimer>
#include <QElapsedTimer>
#include "controller_state.h"

namespace Ui
{
    class ControllerWidget;
}

class Controller;

class ControllerWidget : public QWidget
{
    Q_OBJECT

public:
    ControllerWidget(QWidget* parent = 0);
    ~ControllerWidget();

    Controller* controller() const;
    void setController(Controller* controller);

    bool connectController();
    bool disconnectController();

    bool isPowered() const;

public slots:
    bool reloadController();
    void editController();

    bool turnOn();
    bool turnOff();

    void updatePowerState();

    void showStatus();

signals:
    void controllerUpdated();
    void controllerDeleted();

    void requestPowerUp();
    void requestPowerDown();

    void powerStateChanged();

private slots:
    void beginPowerUp();
    void endPowerUp();
    void powerUp();

    void beginPowerDown();
    void endPowerDown();
    void powerDown();

    void updateState();
    void updateMessage(const QString& message);

private:
    void setupMisc();
    void setupTimers();
    void setupSignals();

private:
    Ui::ControllerWidget* ui;
    Controller* m_controller;

    QTimer m_powerUpTimer;
    QElapsedTimer m_powerUpTimeout;

    QTimer m_powerDownTimer;
    QElapsedTimer m_powerDownTimeout;

    QString m_message;
};

#endif // CONTROLLERWIDGET_H
